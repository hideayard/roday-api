<?php

require_once("tokenlogin.php");

// $secret = "super_secret";
$secret = "B15m1ll4#";

$token = isset($_POST['token']) ? $_POST['token'] : ""; 
$data = isset($_POST['data']) ? $_POST['data'] : ""; 

$otl = new TokenLogin($secret);
$status = false;
$msg = "Please Input Token!";
$hasil = "";
$other = "";
if($token!="")
{

    try {
         $payload = $otl->validate_token($token);
  
      if ($payload) {
            $status = true;
            $msg =  "Valid token!";// You are user #{$payload->uid}";
            $hasil = $payload;
            //action save to DB when token valid
        } else {
            $msg =  "Invalid token";
       }
   } catch (Exception $e) {
        $msg = 'Caught exception: '.  $e->getMessage();
   }
}


echo json_encode( array('status'=> $status,'hasil' => $hasil ,'messages' => $msg,'other' => $data ) );//,'tgl' => $tgl->format('Y-m-d H:i:s')) );
exit;