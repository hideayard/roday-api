<?php
 $token = isset($_POST['token']) ? $_POST['token'] : ""; 
 require_once("tokenlogin.php");
$secret = "B15m1ll4#";

$status = false;
$msg = "Please Input Token!";
if (json_last_error() === JSON_ERROR_NONE) {
    $status = true;
    $msg = "JSON OK";
} else {
    $status = false;
    $msg = "JSON ERROR";
}

$otl = new TokenLogin($secret);
if($token!="")
    {
        try {
            $payload = $otl->validate_token($token);
    
        if ($payload) {
                $status = true;
                $msg =  "Valid token!";
                //action save to DB when token valid
            } else {
                $status = false;
                $msg =  "Invalid token";
            }
        } catch (Exception $e) {
                $status = false;
                $msg = 'Caught exception: '.  $e->getMessage();
        }
    }
    $info = array();
    if($status == true)
    {
        $info += array("uname" => $payload->uname);
        $info += array("unama" => $payload->unama);
        $info += array("ufoto" => $payload->ufoto);
        $info += array("stock" => $payload->stock);
        $info += array("report" => $payload->report);
        $info += array("cash_register" => $payload->cash_register);
    }
    else
    {
        $msg = "Please check token or try to login again!";
        
    }
echo json_encode( array("status" => $status,"info" => $info,"messages" => $msg ) );

?>