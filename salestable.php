

<?php
//auto
// $q_column = "SELECT `COLUMN_NAME` FROM `INFORMATION_SCHEMA`.`COLUMNS` WHERE `TABLE_SCHEMA`='smart' AND `TABLE_NAME`='oshe'";
// $d_columns = $db->rawQuery($q_column);
//end of auto

$table = "transaction";
// $txt_field= "user_name,user_nama,user_hp,user_email,user_tipe,user_foto";
// $txt_label = "Username,Nama,HP,Email,Tipe,Foto";
$txt_field= "
ts_no
,ts_date
,tsCreatedByUsername
,ts_price
";

$txt_label = "
Transaction Number
,Date
,Employee
,Price

";
$q_field = explode(",",$txt_field);
$q_label = explode(",",$txt_label);
// $i=1;$q_oshe = "select ".$q_field[0] ." as " .$q_label[0];
// for($i;$i<count($q_field);$i++)
// {
//     $q_oshe .= ",".$q_field[$i] ." as " .$q_label[$i];
// }
// $q_oshe .= " from $table";
// $d_oshe = $db->rawQuery($q_oshe);
?>

<!-- <link rel="stylesheet" href="css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="css/buttons.bootstrap4.min.css"> -->

    <!-- <script src="plugins/jquery/jquery.min.js"></script> -->
<!-- DataTables -->
<!-- <script src="plugins/datatables/jquery.dataTables.js"></script>
<script src="plugins/datatables/dataTables.fixedHeader.min.js"></script>
<script src="plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script> -->
<!-- <script src="dist/js/dataTables.buttons.min.js"></script> -->
  <!-- DataTables -->
  <!-- <link rel="stylesheet" href="plugins/datatables-bs4/css/dataTables.bootstrap4.css"> -->
  <!-- Theme style -->
  <!-- 
  <script src="cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>  -->
  <style>
  div.dataTables_wrapper {
        width: 800px;
        margin: 0 auto;
    }
    .center {
      text-align : center;
      left : 20%;
    }
    </style>
    

  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
  <link rel="stylesheet" href="vendor/fontawesome-free/css/all.min.css">
  <link rel="stylesheet" href="dist/css/jquery.datetimepicker.css">

  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4-4.1.1/jq-3.3.1/jszip-2.5.0/dt-1.10.21/b-1.6.3/b-html5-1.6.3/fh-3.1.7/r-2.2.5/sp-1.1.1/datatables.min.css"/>
 
  <script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
  

  <!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script> -->
<!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script> -->
<!-- <script type="text/javascript" src="https://cdn.datatables.net/v/bs4-4.1.1/jq-3.3.1/jszip-2.5.0/dt-1.10.21/b-1.6.3/b-html5-1.6.3/fh-3.1.7/r-2.2.5/sp-1.1.1/datatables.min.js"></script> -->
<script type="text/javascript" src="https://cdn.datatables.net/rowgroup/1.1.2/js/dataTables.rowGroup.min.js"></script>
<script src="dist/js/jquery.datetimepicker.js"></script>



          <div class="card">
            <div class="card-header">
              <!-- <h3 class="card-title">List Data oshe</h3> -->
              <!-- <button onclick="" type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-default">
                  Absensi
                </button> -->
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                          <div class="col-md-6 center">
                            <div class="form-group ">
                                <label>Tarikh:</label>
                                <div class="input-group">
                                  <div class="input-group-prepend">
                                    <span class="input-group-text">
                                      <i class="far fa-calendar-alt"></i>
                                    </span>
                                  </div>
                                  <input value="<?=(new \DateTime())->format('Y-m-d')?>"  name="tgl"  type="text" class="form-control float-right" id="tgl">
                                  <span class="input-group-append">
                                  <button type="button" id="btnpilih" class="btn btn-primary">Pilih</button>
                                  <button type="button" id="btnreset" onclick="resettgl();" class="btn btn-secondary">Reset</button>
                                  </span>
                                </div>
                                <!-- <input type="text" class="form-control" placeholder="TARIKH" value=""> -->
                            </div>
                          </div>
                        
              <table id="example2" class="table table-bordered table-hover display nowrap" style="width:100%">
              
                <thead>
                <tr>
                
                <th>No</th>
                <th>Action</th>
                  <?php
                    foreach ($q_label as $key => $value) {
                      echo "<th>".$value."</th>";
                      // var_dump($value);
                    }
  
                  ?>
                </tr>
                </thead>
                <tbody>
               
                </tbody>
                <!-- <tfoot>
                <tr> -->
                <?php
                    // foreach ($q_label as $key => $value) {
                    //   echo "<th>".$value."</th>";
                    // }
                  ?>
                <!-- </tr>
                </tfoot> -->
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->



 

      <script>

      function resettgl()
      { document.getElementById('tgl').value=""; }
      
      $(document).ready( function () {
    // $('#example2').DataTable();

      var tabel = $('#example2').DataTable({
      "orderCellsTop": true,
      "fixedHeader": true,
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "processing": true,
      "serverSide": true,
      "scrollX": true,
      "scrollY": "500px",
      "scrollCollapse": true,
      // "dom": 'Bfrtip',
      "order": [[ 3, "desc" ]],

      "columnDefs": [ {"searchable": false,"orderable": false,"targets": 0 } 
                      ,{"searchable": false,"orderable": false,"targets": 1 } ],
      "ajax": "get_data_sales.php?mode=list"
      ,"rowGroup": {
            "startRender": null,
            "endRender": function ( rows, group ) {
                var salesTotal = rows
                    .data()
                    .pluck(5)
                    .reduce( function (a, b) {
                        return a + b.replace(/[^\d]/g, '')*1;
                    }, 0) ;
                salesTotal = $.fn.dataTable.render.number(',', '.', 0, 'RM ').display( salesTotal );
 
                return $('<tr/>')
                    .append( '<td colspan="5" class="bg-info text-white"><div class=""><strong>Total for '+group+'</strong></div></td>' )
                    .append( '<td class="bg-info text-white"><strong>'+salesTotal+'</strong></td>' );
            },
            dataSrc: 3
        }
    }); //end of datatables


    $('#tgl').datetimepicker({
      timepicker:false,
    //   format:'d-m-Y',
      format:'Y-m-d',
	formatDate:'Y/m/d',
  defaultDate: new Date() 
	// minDate:'-1970/01/02', // yesterday is minimum date
    });

    $('#btnpilih').click( function() {
    // console.log("onchange");
    var tgl = $('#tgl').val();
    // var tgl= new Date(d);  // getting search input value
    // var datestring = tgl.getFullYear()  + "-" + (tgl.getMonth()+1) + "-" + tgl.getDate();
    tabel.columns(3).search(tgl).draw();
    } );
    // document.getElementById('btnpilih').click();
} );
      </script>