<?php
 
/*
 * DataTables example server-side processing script.
 *
 * Please note that this script is intentionally extremely simple to show how
 * server-side processing can be implemented, and probably shouldn't be used as
 * the basis for a large complex system. It is suitable for simple use cases as
 * for learning.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 */
 
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */
include_once ("config/db.php");

// DB table to use
$table = 'purchase';
 
// Table's primary key
$primaryKey = 'ps_id';
 
// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes
/*

*/
$i=-1;
$count=0;
$columns = array(
    array(
        'db'        => 'ps_id',
        'dt'        => ++$i,
        'formatter' => function( $d, $row ) {
            global $count;
                return ++$count;
        }
    )   
    ,array(
        'db'        => 'ps_no',
        'dt'        => ++$i//,
        // 'formatter' => function( $d, $row ) {
        //     $tipe = isset($_SESSION['t']) ? $_SESSION['t'] : "";
        //     $mode = isset($_GET['mode']) ? $_GET['mode'] : ""; 

        //     switch($mode)
        //     {
               
        //         case "list" : {
        //             switch($d)
        //             {
        //                 case "0" : {return '<i class="fa fa-circle text-danger"></i> '."Tidak Masuk";}break;
        //                 case "1" : {return '<i class="fa fa-circle text-warning"></i> '."Sudah Absen";}break;
        //                 case "2" : {return '<i class="fa fa-circle text-success"></i> '."Masuk";}break;
        //             }
        //         }break;                
        //     }

        // }
    )    
    ,array( 'db' => 'ps_tgl',   'dt' => ++$i 
            ,'formatter' => function( $d, $row ) {
                if($d)
                {
                    return (new \DateTime($d))->format('Y-m-d');
                    // return (new \DateTime($d))->format('d-m-Y');
                }
                else
                {
                    return "-";
                }
            }
        )
    ,array( 'db' => 'psCreatedByUsername',   'dt' => ++$i )
    ,array( 'db' => 'ps_item',   'dt' => ++$i )
    ,array( 'db' => 'ps_stock',   'dt' => ++$i )
    // ,array( 'db' => 'ps_price',   'dt' => ++$i )
    ,array( 'db' => 'ps_price',   'dt' => ++$i 
            ,'formatter' => function( $d, $row ) {
                if($d)
                {
                    // return (new \DateTime($d))->format('Y-m-d');
                    return "RM ".$d;
                }
                else
                {
                    return "-";
                }
            }
        )
    
    // ,array( 'db' => 'absen_mandor',   'dt' => ++$i )

    // ,array(
    //     'db'        => 'salary',
    //     'dt'        => 5,
    //     'formatter' => function( $d, $row ) {
    //         return '$'.number_format($d);
    //     }
    // )
);
 

 
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */
 
require( 'ssp.class.php' );
 
echo json_encode(
    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
);