<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();
require_once ('config/MysqliDb.php');
include_once ("config/db.php");
$db = new MysqliDb ('localhost', $dbuser, $dbpass, $dbname);
$db2 = new MysqliDb ('localhost', $dbuser, $dbpass, $dbname);
include("config/functions.php");  
require_once("tokenlogin.php");
$db->startTransaction();
$db2->startTransaction();
// $secret = "super_secret";
$secret = "B15m1ll4#";




// echo json_encode( array('status'=> $status,'hasil' => $hasil ,'msg' => $msg,'other' => $data ) );//,'tgl' => $tgl->format('Y-m-d H:i:s')) );

 

$file = basename($_SERVER['PHP_SELF']);
$filename = (explode(".",$file))[0];
// if(!check_role($filename,''))
// {
//   echo json_encode( array("status" => false,"info" => $_SESSION['t'] ." => ".$filename ,"messages" => "You are not authorized.!!!" ) );
// }
// else
{
    $token = isset($_POST['token']) ? $_POST['token'] : ""; 
    $mode = isset($_POST['mode']) ? $_POST['mode'] : "Register"; 
    $item_name = isset($_POST['item_name']) ? $_POST['item_name'] : ""; 
    $item_qty = isset($_POST['item_qty']) ? $_POST['item_qty'] : ""; 
    $item_price = isset($_POST['item_price']) ? $_POST['item_price'] : ""; 
    $item_remark = isset($_POST['item_remark']) ? $_POST['item_remark'] : ""; 
    
    // $data = json_decode($jsondata);
    $status = false;
    $msg = "Please Input Token!";
    if (json_last_error() === JSON_ERROR_NONE) {
        $status = true;
        $msg = "JSON OK";
        //do something with $json. It's ready to use
    } else {
        $status = false;
        $msg = "JSON ERROR";
    }
// var_dump($data->{'data'}[0]->{'item_name'});die;
// echo count($data->{'data'});die;
    $otl = new TokenLogin($secret);
    if($token!="")
    {
        try {
            $payload = $otl->validate_token($token);
    
        if ($payload) {
                $status = true;
                $msg =  "Valid token!";// You are user #{$payload->uid}";
                // $hasil = $payload;
                //action save to DB when token valid
            } else {
                $status = false;
                $msg =  "Invalid token";
            }
        } catch (Exception $e) {
                $status = false;
                $msg = 'Caught exception: '.  $e->getMessage();
        }
    }

    if($status == true)
    {
        $id_user = $payload->uid;
        $tipe = $payload->utipe;
      
        // $mode = isset($_POST['mode']) ? $_POST['mode'] : ""; 
        // $type = isset($_POST['type']) ? $_POST['type'] : ""; 
      
          switch($mode)
          {
            case "submit" : {$transaction_status = 1;}break;
            case "delete" : {$transaction_status = 0;}break;
            default : {$transaction_status = 1;}break;
          }
      
        $messages = "Register Sukses!!";
        //   $tgl = (new \DateTime())->format('Y-m-d H:i:s');
        $ps_no = $payload->uid .".". round(microtime(true) * 1000);

        $qdata = Array(); 
        $total = 0; 
        $mode = isset($_POST['mode']) ? $_POST['mode'] : "Register"; 
        if($mode == "Register")
        {
            $qdata = Array (  "item_id" => null,
                            "item_code" => $ps_no,
                            "item_name" => $item_name,//$value->{'ps_item'},
                            "item_stock" => $item_qty,//$value->{'ps_qty'},
                            "item_price" => $item_price,//$value->{'ps_price'},
                            "item_remark" => $item_remark,//$value->{'ps_price'},
                            "item_created_by" => $id_user,//$value->{'ps_price'},
                            "itemCreatedByUsername" => $payload->uname,//$value->{'ps_price'},
                            "item_status" => "1"
                        );
                $ids = $db->insert ('items', $qdata);
                if($ids) {
                    $info = "Registered ".$item_name ." with ".$item_qty ." qty";
                    $status = true;$messages = "Register Success!!";
                    $sdata = Array (  "slId" => null,
                            "slUser" => $id_user,
                            "slUsername" => $payload->uname,
                            "slType" => "Register",
                            "slItemCode" => $ps_no,
                            "slItemName" => $item_name,
                            "slQty" => $item_qty,
                            "slPrice" => $item_price,
                            "slRemark" => $item_remark,
                            "slStatus" => "1"
                        );
                        $hstock = $db2->insert ('stock_log', $sdata);
                }
                else {$status = false;$messages = "Register Failed!!";}
                
        }
        else
        {
            $qdata = Array (  "ps_id" => null,
                                "ps_no" => $ps_no,
                                "ps_user" => $id_user,
                                "ps_item" => $item_name,
                                "ps_stock" => $item_qty,
                                "ps_price" => $item_price,
                                "ps_remark" => $item_remark,
                                "psCreatedBy" => $id_user,
                                "psCreatedByUsername" => $payload->uname,
                                "ps_status" => "1"
                            );
                    $ids = $db->insert ('purchase', $qdata);
                    $messages = "Purchase Success!!";
                    if($ids) 
                    {

                        $data = Array (
                            'item_stock' => $db->inc($item_qty),
                            // 'active' => $db->not()
                            "item_status" => 1
                        );
                        $db->where ('item_name', trim($item_name));
                        if ($db->update ('items', $data))
                            {
                                $info = "Added ".$item_qty ." items";
                                $status = true;$messages = "Purchase Success!!";
                                $sdata = Array (  "slId" => null,
                                "slUser" => $id_user,
                                "slUsername" => $payload->uname,
                                "slType" => "Purchase",
                                "slItemCode" => $ps_no,
                                "slItemName" => $item_name,
                                "slQty" => $item_qty,
                                "slPrice" => $item_price,
                                "slRemark" => $item_remark,
                                "slStatus" => "1"
                            );
                            $hstock = $db2->insert ('stock_log', $sdata);
                        }
                        else
                            {$status = false;echo 'Purchase failed: ' . $db->getLastError();}

                    } else {
                        $messages = "Purchase failed!!";$status = false;
                    }

        }
       
            if($status == false) {
                // echo 'insert failed: ' . $db->getLastError();
                echo json_encode( array("status" => false,"info" => $db->getLastError(),"messages" => $messages ) );
                $db->rollback(); $db2->rollback();
            } else {
                $db->commit(); $db2->commit();
                // echo 'new users inserted with following id\'s: ' . implode(', ', $ids);
                echo json_encode( array("status" => true,"info" => $info,"messages" => $messages ) );
            }
    }
    else
    {
        echo json_encode( array("status" => false,"info" => "Please check token or try to login again!","messages" => $msg ) );
    }
 

}

// $db->disconnect();
// $db2->disconnect();

?>