<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();
require_once ('config/MysqliDb.php');
include_once ("config/db.php");
$db = new MysqliDb ('localhost', $dbuser, $dbpass, $dbname);
$db2 = new MysqliDb ('localhost', $dbuser, $dbpass, $dbname);
$db->startTransaction();
$db2->startTransaction();

include("config/functions.php");  
require_once("tokenlogin.php");

// $secret = "super_secret";
$secret = "B15m1ll4#";




// echo json_encode( array('status'=> $status,'hasil' => $hasil ,'msg' => $msg,'other' => $data ) );//,'tgl' => $tgl->format('Y-m-d H:i:s')) );

 

$file = basename($_SERVER['PHP_SELF']);
$filename = (explode(".",$file))[0];
// if(!check_role($filename,''))
// {
//   echo json_encode( array("status" => false,"info" => $_SESSION['t'] ." => ".$filename ,"messages" => "You are not authorized.!!!" ) );
// }
// else
{
    $token = isset($_POST['token']) ? $_POST['token'] : ""; 
    $jsondata = isset($_POST['jsondata']) ? $_POST['jsondata'] : ""; 
    $data = json_decode($jsondata);
    $status = false;
    $msg = "Please Input Token!";
    if (json_last_error() === JSON_ERROR_NONE) {
        $status = true;
        $msg = "JSON OK";
        //do something with $json. It's ready to use
    } else {
        $status = false;
        $msg = "JSON ERROR";
    }
// var_dump($data->{'data'}[0]->{'item_name'});die;
// echo count($data->{'data'});die;
    $otl = new TokenLogin($secret);
    if($token!="")
    {
        try {
            $payload = $otl->validate_token($token);
    
        if ($payload) {
                $status = true;
                $msg =  "Valid token!";// You are user #{$payload->uid}";
                // $hasil = $payload;
                //action save to DB when token valid
            } else {
                $status = false;
                $msg =  "Invalid token";
            }
        } catch (Exception $e) {
                $status = false;
                $msg = 'Caught exception: '.  $e->getMessage();
        }
    }

    if($status == true)
    {
        $id_user = $payload->uid;
        $tipe = $payload->utipe;
      
        $mode = isset($_POST['mode']) ? $_POST['mode'] : ""; 
        $type = isset($_POST['type']) ? $_POST['type'] : ""; 
      
          switch($mode)
          {
            case "submit" : {$transaction_status = 1;}break;
            case "delete" : {$transaction_status = 0;}break;
            default : {$transaction_status = 1;}break;
          }
      
        //   $tgl = (new \DateTime())->format('Y-m-d H:i:s');
        $ts_no = $payload->uid .".". round(microtime(true) * 1000);
        $qdata = $sdata = $logdata =  Array(); 
        $total = 0; 
        $info = "";
        $totalupdated = 0;
            foreach($data->{'data'} as $key => $value)
            {   
                $total += ($value->{'ts_qty'} * $value->{'ts_price'});
                $qdata[] = Array (  "ts_id" => null,
                                    "ts_no" => $ts_no,
                                    "ts_created_by" => $id_user,
                                    "ts_item" => $value->{'ts_item'},
                                    "ts_qty" => $value->{'ts_qty'},
                                    "ts_price" => $value->{'ts_price'},
                                    "ts_type" => 2,
                                    "tsCreatedByUsername" => $payload->uname,
                                    "ts_status" => "1"
                                );
                $qty = $value->{'ts_qty'};
                $sdata = Array (  'item_stock' => $db->inc(-($value->{'ts_qty'})) );
                $db->where ('item_name', trim($value->{'ts_item'}) );
                if ($db->update ('items', $sdata))
                {
                    $totalupdated++;
                    $info =  $totalupdated . ' items were updated';
                    $logdata[] = Array (  "slId" => null,
                                    "slType" => "SALES",
                                    "slItemName" => $value->{'ts_item'},
                                    "slQty" => $qty,
                                    "slPrice" => $value->{'ts_price'},
                                    "slUser" => $payload->uid,
                                    "slUsername" => $payload->uname
                                );
                }
                else
                {
                    $info = 'update failed: ' . $db->getLastError();
                }
                    
            }
            // var_dump($logdata);
            //header
            $qdata[] = Array (  "ts_id" => null,
                            "ts_created_by" => $id_user,
                            "ts_no" => $ts_no,
                            "ts_qty" => count($data->{'data'}),
                            "ts_price" => $total,
                            "ts_type" => 1,
                            "ts_remark" => $data->{'remark'},
                            "tsCreatedByUsername" => $payload->uname,
                            "ts_status" => "1"
                        );
            $ids = $db->insertMulti('transaction', $qdata);
            if(!$ids) {
                // echo 'insert failed: ' . $db->getLastError();
                echo json_encode( array("status" => false,"info" => $db->getLastError(),"messages" => $msg ) );

            } else {
                // echo 'new users inserted with following id\'s: ' . implode(', ', $ids);
                if($db2->insertMulti('stock_log', $logdata))
                {   $db->commit(); $db2->commit();
                    $info .=  ' , Log Saved'; $msg = "Transaction Success.!";
                    echo json_encode( array("status" => true,"info" => $info,"messages" => $msg ) );
                }
                else
                {
                    $db->rollback(); $db2->rollback();
                    $msg = "Transaction Failed.!";
                    echo json_encode( array("status" => false,"info" => $db2->getLastError(),"messages" => $msg ) );

                }
                
                
            }
    }
    else
    {
        echo json_encode( array("status" => false,"info" => "Please check token or try to login again!","messages" => $msg ) );
    }
 

}

// $db->disconnect();
// $db2->disconnect();
?>